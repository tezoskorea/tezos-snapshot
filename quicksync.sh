usage() {
  echo "Usage: $0 <carthagenet|mainnet>"
}

case "$1" in
  carthagenet)
    ;;
  mainnet)
    ;;
  *) usage
    exit 1
    ;;
esac

NET=$1
MAIN="200413_907051_BLn6A1mQF"
CARTHA="200623_506559_BMVQwSPp"

if [ $NET -eq mainnet];
then
  TARGET=${NET}_${MAIN}
else
  TARGET=${NET}_${CARTHA}
fi


echo ================================
echo Downloading a snapshot file
echo ================================

wget -c -O ${TARGET}.tar.gz https://gitlab.com/tezoskorea/tezos-snapshot/raw/master/${TARGET}.tar.gz

rm -rf ~/.tezos-node/context
rm -rf ~/.tezos-node/store
rm -rf ~/.tezos-node/lock

echo ================================
echo Extracting the snapshot file
echo ================================
tar -zxvf ${TARGET}.tar.gz

echo ================================
echo Importing the snapshot file
echo ${TARGET}
echo ================================
SNAP_DIR=$(pwd)
cd ~/tezos
NODE='tezos-node'
if [ -e "$NODE" ];
then
  ./tezos-node snapshot import ${SNAP_DIR}/${TARGET}
else
  tezos-node snapshot import ${SNAP_DIR}/${TARGET}
fi

cd ~/.tezos-node
IDENTITY='identity.json'
if [ -e "$IDENTITY" ];
then
  cd ~/tezos
  echo "======== You have the node identity! ========="
else
  cd ~/tezos
  ./tezos-node identity generate
fi

rm ${SNAP_DIR}/${TARGET}

